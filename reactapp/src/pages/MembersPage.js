//Dependencies
import React, {useState, useEffect} from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTable from '../tables/MemberTable';
import axios from 'axios';


//COMPONENTS
const MembersPage = (props) => { //props - arguments
/*anonymous function*/
//JSX
console.log("memberpage props", props.tokenAttr)

const [ membersData, setMembersData] = useState({
  token: props.tokenAttr,
  members: []
})

const { token, members} = membersData
console.log("MembersPage Token", token)
console.log("MembersPage members", members)

//access members from nodeapp
const getMembers = async () => {
  try {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }

    const res = await axios.get("http://localhost:4051/members", config)

    console.log("Members Res", res)
    return setMembersData({
      ...membersData,
      members: res.data
    })

  } catch(e) {
    console.log(e.response)
    //swal
  }
}

//deleting a member by an admin
const deleteMember = async (id) => {
  try {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }

    const res = await axios.delete(`http://localhost:4051/members/${id}`, config)
    console.log("Delete testing", res.data)
    getMembers()
  } catch(e) {
    //SWAL
    console.log(e)
  }
}

useEffect(()=> {
  getMembers()
}, [setMembersData])


  return (
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Members Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4" className="border"><MemberForm/></Col>
        <Col><MemberTable membersAttr={members} deleteMember={deleteMember}/></Col>
      </Row>
    </Container>
  );
}

export default MembersPage;

//Discussion
//1.) What is a component?
// - function or class.
// - contains props (arguments)