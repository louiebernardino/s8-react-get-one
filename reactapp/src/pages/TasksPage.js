//Dependencies
import React, {useState, useEffect} from 'react';
import { Container, Row, Col } from 'reactstrap';
import TaskForm from '../forms/TaskForm';
import TaskTable from '../tables/TaskTable';
import axios from 'axios';


//COMPONENTS
const TasksPage = (props) => { //props - arguments
/*anonymous function*/
//JSX

console.log("taskpage props", props.tokenAttr)

const [ tasksData, setTasksData] = useState({
  token: props.tokenAttr,
  tasks: []
})

const { token, tasks} = tasksData
console.log("TasksPage Token", token)
console.log("TasksPage tasks", tasks)

//access members from nodeapp
const getTasks = async () => {
  try {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    }

    const res = await axios.get("http://localhost:4051/tasks", config)

    console.log("tasks Res", res)
    return setTasksData({
      ...tasksData,
      tasks: res.data
    })

  } catch(e) {
    console.log(e.response)
    //swal
  }
}

useEffect(()=> {
  getTasks()
}, [setTasksData])

// console.log("Task", props.getCurrentMemberAttr)
  return (
    <Container>
      <Row className="mb-4">
        <Col>
          <h1>Tasks Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md="4" className="border"><TaskForm/></Col>
        <Col><TaskTable tasksAttr={tasks}/></Col>
      </Row>
    </Container>
  );
}

export default TasksPage;

//Discussion
//1.) What is a component?
// - function or class.
// - contains props (arguments)