import React from 'react';
import { Table } from 'reactstrap';
import TaskRow from '../rows/TaskRow'
import { Container, Row, Col } from 'reactstrap';


const TaskTable = (props) => {

console.log(props.tasksAttr)

let row;

if(!props.tasksAttr) {
  row = (
    <tr>
      <td colSpan="3">
        <em>No tasks found...</em>
      </td>
    </tr>
    )
  } else {
    let i = 0
    row = (
        props.tasksAttr.map(task => {
          return <TaskRow taskAttr={task} key={task._id} index={++i}/>
        })

      )
  }

  return (
    <Table hover borderless sie="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Member Id</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
      </thead>
        <tbody>
          {row}
        </tbody>
    </Table>
  );
}

export default TaskTable;