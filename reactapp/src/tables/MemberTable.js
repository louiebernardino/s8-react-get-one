import React from 'react';
import { Table } from 'reactstrap';
import MemberRow from '../rows/MemberRow'
import { Container, Row, Col } from 'reactstrap';


const MemberTable = (props) => {

console.log(props.membersAttr)

let row;

if(!props.membersAttr) {
  row = (
    <tr>
      <td colSpan="3">
        <em>No members found...</em>
      </td>
    </tr>
    )
  } else {
    let i = 0
    row = (
        props.membersAttr.map(member => {
          return <MemberRow memberAttr={member} key={member._id} index={++i} deleteMember={props.deleteMember }/>
        })

      )
  }

  return (
    <Table hover borderless sie="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Email</th>
          <th>Team</th>
          <th>Position</th>
          <th>Status</th>
          <th>Action</th>
        </tr>
      </thead>
        <tbody>
         {row}
        </tbody>
    </Table>
  );
}

export default MemberTable;