import React from 'react';
import { Table } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import TeamRow from '../rows/TeamRow';


const TeamTable = (props) => {
console.log(props.teamsAttr)

let row;

if(!props.teamsAttr) {
  row = (
    <tr>
      <td colSpan="3">
        <em>No teams found...</em>
      </td>
    </tr>
    )
  } else {
    let i = 0
    row = (
        props.teamsAttr.map(team => {
          return <TeamRow teamAttr={team} key={team._id} index={++i}/>
        })

      )
  }


  return (
    <Table hover borderless sie="sm" className="p-4 rounded">
      <thead>
        <tr>
          <th>#</th>
          <th>Team</th>
          <th>Username</th>
          <th>Email</th>
          <th>Position</th>
          <th>Action</th>
        </tr>
      </thead>
        <tbody>
         {row}
        </tbody>
      <thead>
      </thead>
    </Table>
  );

}

export default TeamTable;