import React, {Fragment} from 'react';
import { Table } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import {Link} from 'react-router-dom'


const MemberRow = ({memberAttr, index, deleteMember}) => {
const { firstName, lastName, username, email, _id, teamId, isActive } = memberAttr
  return (
  	<Fragment>
  		<tr>
          <th scope="row">{ index }</th>
          <td>{ username }</td>
          <td>{ teamId ? teamId.name : "N/A" }</td>
          <td>Team 1</td>
          <td>Admin</td>
          <td>{isActive ? "Active": "Deactivated"}</td>
          <td><Link size="sm" className="btn btn-info" to={`/members/${_id}`}><i className="fas fa-eye"></i></Link>
          <Button className="ml-1 mr-1" color="warning"><i className="fas fa-edit"></i></Button>
          <Button color="danger"><i className="fas fa-trash-alt" onClick={()=> deleteMember(_id)}></i></Button></td>
        </tr>
    </Fragment>
	);
}

export default MemberRow;

