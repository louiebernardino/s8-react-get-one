import React, {Fragment} from 'react';
import { Table } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';


const TaskRow = ({taskAttr, index}) => {
const { description, _id, memberId} = taskAttr
  return (
  	<Fragment>
  		<tr>
          <th scope="row">{ index }</th>
          <td>{ memberId ? memberId.username : "N/A"  }</td>
          <td>{ memberId ? memberId._id : "N/A"}</td>
          <td>{ description }</td>
          <td><Button size="sm" color="primary"><i className="fas fa-eye"></i></Button>
          <Button size="sm" className="ml-1 mr-1" color="warning"><i className="fas fa-edit"></i></Button>
          <Button size="sm" color="danger"><i className="fas fa-trash-alt"></i></Button></td>
        </tr>
    </Fragment>
	);
}

export default TaskRow;

